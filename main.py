#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl    '
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


class Kebab: pass


kebab = Kebab()


print(kebab)
print(isinstance(kebab, Kebab))
print(isinstance(kebab, object))


class ZWolowina(Kebab): pass


class ZBaranina(Kebab): pass


wolowina = ZWolowina()
baranina = ZBaranina()


print(wolowina)
print(isinstance(wolowina, ZWolowina))
print(isinstance(wolowina, Kebab))
print(isinstance(wolowina, object))

print(baranina)
print(isinstance(baranina, ZBaranina))
print(isinstance(baranina, Kebab))
print(isinstance(baranina, object))

print(isinstance(wolowina, ZBaranina))
print(isinstance(baranina, ZWolowina))
