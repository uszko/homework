#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl'
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


name: str = input("Give me a name")
lastname: str = input("Give me a lastname")
age: int = input("Give me an age")

print(name, lastname, age)


def conv() -> None:
    int_number: str = input("Give me some integer")
    float_number: str = input("Give me some integer")

    int_number: int = int(int_number)
    float_number: float = float(float_number)

    print("int number {}.".format(int_number))
    print("float number {}.".format(float_number))


conv()


class Kwiat:
    def __init__(self, nazwa: str, gatunek: str, rodzaj: str):
        self.nazwa = nazwa
        self.gatunek = gatunek
        self.rodzaj = rodzaj

    def info(self):
        print("Nazwa kwiata : {} , rodzaj: {} , gatunek: {}.".format(
            self.nazwa, self.rodzaj, self.gatunek
        ))


raw_nazwa: str = input("Podaj nazwe kwiata")
raw_rodzaj: str = input("Podaj rodzaj kwiata")
raw_gatunek: str = input("Podaj gatunek kwiata")

kwiat = Kwiat(raw_nazwa, raw_rodzaj, raw_gatunek)
kwiat.info()
