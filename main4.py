#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl'
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


def check_name(name: str) -> str:
    my_name: str = "Wiktor"
    if name != my_name:
        print("Hello there {}.".format(name))
    else:
        print("Hi {}.".format(my_name))


raw_name: str = input("Give me a name")
check_name(raw_name)


class Rectangle:
    def __init__(self, lenght: int, width: int, name:str):
        """ Const """
        self.length: int = lenght
        self.width: int = width
        self.name: str = name

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return (self.length == other.length) and (self.width == other.width)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return self.length*self.width < other.length*other.width

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return self.length*self.width > other.length*other.width

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return self.length*self.width <= other.length*other.width

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return self.length*self.width >= other.length*other.width

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return self.length*self.width != other.length*other.width


a = Rectangle(5, 4, "A")
b = Rectangle(5, 4, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}".format(a != b))


class Cuboid:
    def __init__(self, lenght: int, width: int, height: int, name: str):
        """ Const """
        self.length: int = lenght
        self.width: int = width
        self.name: str = name
        self.height: int = height

    def __eq__(self, other):
        print("Comparision type: {} == {}".format(self.name, other.name))
        return (self.length * self.width * self.height) == (other.length * other.width * other.height)

    def __lt__(self, other):
        print("Comparision type: {} < {}".format(self.name, other.name))
        return (self.length * self.width * self.height) < (other.length * other.width * other.height)

    def __gt__(self, other):
        print("Comparision type: {} > {}".format(self.name, other.name))
        return (self.length * self.width * self.height) > (other.length * other.width * other.height)

    def __le__(self, other):
        print("Comparision type: {} <= {}".format(self.name, other.name))
        return (self.length * self.width * self.height) <= (other.length * other.width * other.height)

    def __ge__(self, other):
        print("Comparision type: {} >= {}".format(self.name, other.name))
        return (self.length * self.width * self.height) >= (other.length * other.width * other.height)

    def __ne__(self, other):
        print("Comparision type: {} != {}".format(self.name, other.name))
        return (self.length * self.width * self.height) != (other.length * other.width * other.height)


A = Cuboid(5, 4, 10, "A")
B = Cuboid(5, 4, 10, "B")


print("1. A == B = {}".format(A == B))
print("2. A < B = {}".format(A < B))
print("3. A > B = {}".format(A > B))
print("4. A <= B = {}".format(A <= B))
print("5. A >= B = {}".format(A >= B))
print("6. A != B = {}".format(A != B))
