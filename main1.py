#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl'
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


def name() -> str:
    print("Wiktor")


def sum() -> int:
    print(2 + 2 + 2)


def hi_world() -> str:
    return "Czesc swiecie"


def sub(a: int, b: int) -> int:
    return a - b


class Beer: pass


def create_beer() -> Beer:
    return Beer()


name()
sum()
print(hi_world())
print(sub(10, 5))
new_beer = create_beer()
print(type(new_beer))
