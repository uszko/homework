#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl'
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


numbers = [
    [1], [2], [3]
]

for num in numbers:
    print(num)


empty = []

for emp in range(100, 115):
    empty.append(emp)

print(empty)


numbs: float = [5.12, 5.15, 7.73, 1.52, 7.12]

for numb in numbs:
    print(numb)
