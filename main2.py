#!/usr/bin/env python3

__version__ = '1.0'
__author__ = 'Wiktor Polanowski'
__email__ = 'wik-pol10@wp.pl'
__copyright___ = "Copyright (c) 2020 Wiktor Polanowski"


class President:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname
        print("President with name {} {} .".format(self.firstname, self.lastname))

    def ability_to_think(self) -> None:
        print("I have ability to think")

class Poland(President):
    def __init__(self, *args):
        super(Poland, self).__init__(*args)
        print("Run")

    def ability_to_think(self) -> None:
        print("Not sure about this one")


# president = President("","")
endrju = Poland("Andrzej", "Duda")
endrju.ability_to_think()


